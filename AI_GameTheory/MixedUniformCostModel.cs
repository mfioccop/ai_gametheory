﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace AI_GameTheory
{
	public class MixedUniformCostModel : BaseFraudGameModel
	{
		public MixedUniformCostModel(int attackerPayoff, int attackVectorLength, Attack attackProfile)
		{
			AttackerPayoff = attackerPayoff;
			AttackVector = new List<Attack>(attackVectorLength);
			for (int i = 0; i < AttackVector.Capacity; i++)
			{
				Attack a = new Attack(attackProfile.id, attackProfile.costAttack, attackProfile.costDefend, attackProfile.payoffAttack, attackProfile.payoffDefend);
				AttackVector.Add(a);
			}

			Attacked = new List<Attack>(attackVectorLength);
			Defended = new List<Attack>(attackVectorLength);

		}


		public int AttackerPayoff { get; set; }


		/// <summary>
		/// Chooses the attacks that the attacker will execute.
		/// </summary>
		/// <param name="strategy">The strategy that the attacker will use to choose the attacks.</param>
		/// <exception cref="ArgumentException">strategy value must be a non-negative number.</exception>
		public override void ChooseAttacks(int strategy)
		{
			if (strategy < 0)
			{
				throw new ArgumentException("strategy value must be a non-negative number.");
			}

			List<Attack> allAttacks = AttackVector;
			List<Attack> attacks = new List<Attack>();
			Random r = new Random();

			for (int ii = 0; ii < strategy; ii++)
			{
				if (allAttacks.Count == 0)
				{
					break;
				}
				// Generate a random element to skip to and choose
				int skip = r.Next(0, allAttacks.Count);
				Attack choice = allAttacks.Skip(skip).Take(1).First();

				// Remove the choice from the pool and add it to the attacker's list
				allAttacks.Remove(choice);
				attacks.Add(choice);
			}

			Attacked = attacks;
		}

		/// <summary>
		/// Chooses the attacks that the defender will try to defend against.
		/// </summary>
		/// <param name="strategy">The strategy that the defender will use to choose the attacks to defend against.</param>
		/// <exception cref="ArgumentException">strategy value must be a non-negative number.</exception>
		public override void ChooseDefense(int strategy)
		{
			if (strategy < 0)
			{
				throw new ArgumentException("strategy value must be a non-negative number.");
			}

			List<Attack> allAttacks = AttackVector;
			List<Attack> defense = new List<Attack>();
			Random r = new Random();

			for (int ii = 0; ii < strategy; ii++)
			{
				if (allAttacks.Count == 0)
				{
					break;
				}
				// Generate a random element to skip to and choose
				int skip = r.Next(0, allAttacks.Count);

				Attack choice = allAttacks.Skip(skip).Take(1).FirstOrDefault();

				// Remove the choice from the pool and add it to the defender's list
				allAttacks.Remove(choice);
				defense.Add(choice);
			}

			Defended = defense;
		}


		public Tuple<int, int>[,] GenerateAttackPayoffMatrix(int attackerCost, int attackerPayoff, int defenderCost, int defenderPayoff, int attackVectorLength)
		{
			// attackVectorLength+1 accounts for the cases where attacker/defender don't choose any
			Tuple<int, int>[,] matrix = new Tuple<int, int>[attackVectorLength + 1, attackVectorLength + 1];

			// Iterate through all attack strategies
			for (int attStrategy = 1; attStrategy <= attackVectorLength; attStrategy++)
			{
				// Attacker probability for intersection
				float n = (attStrategy / 100);

				// For each attack strategy use all defence strategies
				for (int defStrategy = 1; defStrategy <= attackVectorLength; defStrategy++)
				{
					float m = defStrategy / 100;

					float probability = 1 - m * n;	// probability that there are no intersections between attacker and defender sets

					int attackerProfit = 0;
					int attackerRoundPayoff = 0;
					int defenderProfit = 0;

					if (defStrategy == 0) // defender has chosen not to defend anything, attacker will always succeed
					{
						attackerProfit = (attackerPayoff * attStrategy) - (attackerCost * attStrategy); // multiply profit by the average successful attacks
						attackerRoundPayoff = attackerPayoff * attStrategy; // need this for the defenderProfit
					}
					else if (attStrategy + defStrategy > attackVectorLength) // pigeon-hole principle for intersections between the sets
					{
						attackerProfit = -(attackerCost * attStrategy);
						attackerRoundPayoff = 0;
					}
					else // expected payoff is (payoff of an attack * probability of no intersection * number of attacks) - costs
					{
						attackerProfit = (int)(attackerPayoff * probability * attStrategy) - (attackerCost * attStrategy);
						attackerRoundPayoff = (int)(attackerPayoff * probability * attStrategy); // need this for the defenderProfit
					}

					defenderProfit = -attackerRoundPayoff - (defStrategy * defenderCost);

					// Add the game to the matrix
					Tuple<int, int> matrixCell = new Tuple<int, int>(attackerProfit, defenderProfit);
					matrix[attStrategy - 1, defStrategy - 1] = matrixCell;
				}
			}

			OutputPayoffMatrixCSV(matrix, attackerCost);

			return matrix;
		}

		private void OutputPayoffMatrix(Tuple<int, int>[,] matrix, int cost)
		{
			string fileName = "MUCM_Matrix_" + cost.ToString() + ".xml";
			XmlWriter matrixXML = XmlWriter.Create(fileName);

			matrixXML.WriteStartDocument();
			matrixXML.WriteStartElement("costMatrix");

			for (int attStrategy = 0; attStrategy < 101; attStrategy++)
			{
				matrixXML.WriteStartElement("row");
				for (int defStrategy = 0; defStrategy < 101; defStrategy++)
				{
					// Build the element name and contents in form <x,y>AttCost, DefCost</x,y> where x is the attack strategy and y is the defence strategy
					string elementName = attStrategy.ToString() + "," + defStrategy.ToString();
					string elementString = matrix[attStrategy, defStrategy].Item1.ToString() + ", " +
										   matrix[attStrategy, defStrategy].Item2.ToString();

					// Write data to the XML document
					matrixXML.WriteStartElement("column");
					matrixXML.WriteAttributeString("attacker", attStrategy.ToString());
					matrixXML.WriteAttributeString("defender", defStrategy.ToString());
					matrixXML.WriteString(elementString);
					matrixXML.WriteEndElement();
				}
				matrixXML.WriteEndElement();
			}

			matrixXML.WriteEndDocument();
			matrixXML.Close();
		}

		private void OutputPayoffMatrixCSV(Tuple<int, int>[,] matrix, int cost)
		{
			string fileName = "MUCM_Matrix_" + cost.ToString() + ".csv";
			string delimeter = ",";

			StringBuilder builder = new StringBuilder();

			for (int attStrategy = 0; attStrategy < matrix.GetUpperBound(0); attStrategy++)
			{
				for (int defStrategy = 0; defStrategy < matrix.GetUpperBound(1); defStrategy++)
				{
					if (defStrategy != 0)
					{
						builder.Append(delimeter);
					}
					builder.Append(matrix[attStrategy, defStrategy].Item1 + "|" + matrix[attStrategy, defStrategy].Item2);
				}

				builder.AppendLine();
			}

			try
			{
				File.WriteAllText(fileName, builder.ToString());
			}
			catch(IOException ioException)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine("\nERROR: Could not write the payoff matrix csv file, make sure all files with the same name are closed.\n");
				Console.ResetColor();
			}
		}

	}
}
