﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AI_GameTheory
{
	/// <summary>
	/// Base model for the fraud game proposed in the CactusCard anti-fraud scenario.
	/// </summary>
	public abstract class BaseFraudGameModel
	{
		/// <summary>
		/// Struct containing the basics of an Attack; consists of an ID to identify the attack, a cost for employing the attack, and a payoff if the attack is successful.
		/// </summary>
		public class Attack : IEquatable<Attack>, IFormattable
		{
			public int id;
			public int costAttack;
			public int costDefend;
			public int payoffAttack;
			public int payoffDefend;
			public static int UNIQUE_ID = 0;

			public Attack()
			{
				id = UNIQUE_ID++;
				costAttack = 100;
				costDefend = 100;
				payoffAttack = 10000;
				payoffDefend = 0;
			}

			public Attack(int id, int costAttack, int costDefend, int payoffAttack, int payoffDefend)
			{
				this.id = UNIQUE_ID++;
				this.costAttack = costAttack;
				this.costDefend = costDefend;
				this.payoffAttack = payoffAttack;
				this.payoffDefend = payoffDefend;
			}

			public bool Equals(Attack other)
			{
				return id == other.id && costAttack == other.costAttack && costDefend == other.costDefend && payoffAttack == other.payoffAttack && payoffDefend == other.payoffDefend;
			}

			public string ToString(string format, IFormatProvider formatProvider)
			{
				return string.Format("attack vector {0}", id);
			}

		}

		/// <summary>
		/// Gets or sets the attack vector.
		/// </summary>
		/// <value>
		/// The attack vector that attackers and defenders can choose from in the game.
		/// </value>
		public List<Attack> AttackVector { get; set; }

		/// <summary>
		/// Gets or sets the attacked.
		/// </summary>
		/// <value>
		/// The attacks that the attacker has chosen.
		/// </value>
		public List<Attack> Attacked { get; protected set; }

		/// <summary>
		/// Chooses the attacks that the attacker will execute.
		/// </summary>
		/// <param name="strategy">The strategy that the attacker will use to choose the attacks.</param>
		public abstract void ChooseAttacks(int strategy);

		/// <summary>
		/// Gets or sets the defended.
		/// </summary>
		/// <value>
		/// The attacks that the defender has chosen to defend against.
		/// </value>
		public List<Attack> Defended { get; protected set; }

		/// <summary>
		/// Chooses the attacks that the defender will try to defend against.
		/// </summary>
		/// <param name="strategy">The strategy that the defender will use to choose the attacks to defend against.</param>
		public abstract void ChooseDefense(int strategy);

		/// <summary>
		/// Gets the successful attacks.
		/// </summary>
		/// <value>
		/// The successful attacks that weren't defended against.
		/// </value>
		public List<Attack> SuccessfulAttacks
		{
			get { return Attacked.Except(Defended).ToList(); }
		}

		/// <summary>
		/// Gets the sum of attacker costs.
		/// </summary>
		/// <value>
		/// The sum of attacker costs.
		/// </value>
		public int SumAttackerCosts
		{
			get { return Attacked.AsEnumerable().Sum(a => a.costAttack); }
		}

		/// <summary>
		/// Gets the sum of defender costs.
		/// </summary>
		/// <value>
		/// The sum of defender costs.
		/// </value>
		public int SumDefenderCosts
		{
			get { return Defended.AsEnumerable().Sum(d => d.costDefend); }
		}

		/// <summary>
		/// Gets the attacker's maximum payoff.
		/// </summary>
		/// <value>
		/// The attacker's maximum payoff.
		/// </value>
		public int AttackerMaxPayoff
		{
			get { return Attacked.AsEnumerable().Sum(a => a.payoffAttack); }
		}

		/// <summary>
		/// Gets the defender's maximum payoff.
		/// </summary>
		/// <value>
		/// The defender's maximum payoff.
		/// </value>
		public int DefenderMaxPayoff
		{
			get { return Attacked.AsEnumerable().Sum(d => d.payoffDefend); }
		}

		/// <summary>
		/// Gets the attacker's gross payoff.
		/// </summary>
		/// <value>
		/// (Sum of payoffs for each successful attack)
		/// </value>
		public int AttackerGrossPayoff
		{
			get { return SuccessfulAttacks.AsEnumerable().Sum(a => a.payoffAttack); }
		}

		/// <summary>
		/// Gets the attacker's net payoff.
		/// </summary>
		/// <value>
		/// (Successful attack payoffs) - (Cost to execute each attack)
		/// </value>
		public int AttackerNetPayoff
		{
			get
			{
				if (Attacked.Intersect(Defended).Any())
				{
					return -SumAttackerCosts;
				}
				else
				{
					return AttackerGrossPayoff - SumAttackerCosts;
				}
			}
		}

		/// <summary>
		/// Gets the defender's gross payoff.
		/// </summary>
		/// <value>
		/// -(AttackerPayoff)
		/// </value>
		public int DefenderGrossPayoff
		{
			get { return -AttackerGrossPayoff; }
		}

		/// <summary>
		/// Gets the defender's net payoff.
		/// </summary>
		/// <value>
		/// -(AttackerPayoff) - (Cost to defend against each attack)
		/// </value>
		public int DefenderNetPayoff
		{
			get
			{
				if (Attacked.Intersect(Defended).Any())
				{
					return -SumDefenderCosts;
				}
				return DefenderGrossPayoff - SumDefenderCosts;
			}
		}

	}
}
