﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AI_GameTheory
{
	public class DeterministicAdversaryWithMemory : BaseFraudGameModel
	{

		private static Random _damRandom;

		public static Random DamRandom
		{
			get
			{
				if (_damRandom == null)
				{
					_damRandom = new Random();
					return _damRandom;
				}
				return _damRandom;
			}
		}

		public int AttackerCosts { get; private set; }
		public List<int> AttackerPayoffs { get; private set; }
		public int DefenderCosts { get; private set; }
		public List<int> DefenderPayoffs { get; private set; }


		private int currentGame;

		/// <summary>
		/// Gets or sets the game count.
		/// </summary>
		/// <value>
		/// The number of times the game is played
		/// </value>
		public int GameCount { get; set; }

		/// <summary>
		/// Gets or sets the attacker memory.
		/// </summary>
		/// <value>
		/// The attacker memory, in number of previous games
		/// </value>
		public int AttackerMemory
		{
			get
			{
				return attackerMemory;
			}
			set
			{
				attackerMemory = value;
				PreviousAttacks = new List<List<Attack>>(attackerMemory);
			}
		}

		private int attackerMemory;

		/// <summary>
		/// Gets or sets the defender memory.
		/// </summary>
		/// <value>
		/// The defender memory, in number of previous games
		/// </value>
		public int DefenderMemory
		{
			get
			{
				return defenderMemory;
			}
			set
			{
				defenderMemory = value;
				PreviousDefends = new List<List<Attack>>(defenderMemory);
			}
		}

		private int defenderMemory;

		/// <summary>
		/// Gets or sets the previous attacks.
		/// </summary>
		/// <value>
		/// A list of the attacker's previously chosen attacks 
		/// </value>
		public List<List<Attack>> PreviousAttacks { get; private set; }

		/// <summary>
		/// Gets or sets the previous defends.
		/// </summary>
		/// <value>
		/// A list of the defender's previously chosen attacks to defend against
		/// </value>
		public List<List<Attack>> PreviousDefends { get; private set; }


		public DeterministicAdversaryWithMemory(int gameCount, int attackVectorLength, Attack attackProfile, int attackerMemory, int defenderMemory)
		{
			if (gameCount < 0 || attackerMemory < 0 || defenderMemory < 0 || gameCount <= attackerMemory || gameCount <= defenderMemory)
			{
				throw new ArgumentOutOfRangeException("gameCount");
			}

			GameCount = gameCount;

			AttackVector = new List<Attack>(attackVectorLength);
			for (int i = 0; i < AttackVector.Capacity; i++)
			{
				Attack a = new Attack(attackProfile.id, attackProfile.costAttack, attackProfile.costDefend, attackProfile.payoffAttack, attackProfile.payoffDefend);
				AttackVector.Add(a);
			}

			AttackerMemory = attackerMemory;
			DefenderMemory = defenderMemory;
			PreviousAttacks = new List<List<Attack>>(attackerMemory);
			PreviousDefends = new List<List<Attack>>(defenderMemory);


		}

		public void RunSimulation(int attackerStrategy, float attackerProbability, int defenderStrategy, float defenderProbability)
		{
			currentGame = 0;

			AttackerPayoffs = new List<int>();
			DefenderPayoffs = new List<int>();

			while (currentGame < GameCount)
			{
				// Attacker doesn't have enough memory yet to choose based on previous games, default to uniform cost
				if (currentGame < attackerMemory)
				{
					ChooseAttacks(attackerStrategy);
					PreviousAttacks.Add(Attacked);
				}
				else
				{
					List<Attack> attacks = chooseAttacksWithMemory(attackerStrategy, attackerProbability);
					if (PreviousAttacks.Count == PreviousAttacks.Capacity)
					{
						PreviousAttacks.RemoveAt(0);
					}
					PreviousAttacks.Add(attacks);
					Attacked = attacks;
				}

				// Defender doesn't have enough memory yet to choose based on previous games, default to uniform cost
				if (currentGame < defenderMemory)
				{
					ChooseDefense(defenderStrategy);
					PreviousDefends.Add(Defended);

				}
				else
				{
					List<Attack> defend = chooseDefenseWithMemory(defenderStrategy, defenderProbability);
					if (PreviousDefends.Count == PreviousDefends.Capacity)
					{
						PreviousDefends.RemoveAt(0);
					}
					PreviousDefends.Add(defend);
					Defended = defend;
				}

				AttackerCosts += Attacked.Sum(a => a.costAttack);
				DefenderCosts += Defended.Sum(d => d.costDefend);

				AttackerPayoffs.Add(AttackerNetPayoff);
				DefenderPayoffs.Add(DefenderNetPayoff);

				currentGame++;
			}

		}

		public void RunAdvancedSimulation(int attackerStrategy, float attackerProbability)
		{
			AttackerPayoffs = new List<int>();
			DefenderPayoffs = new List<int>();

			currentGame = 0;
			DAMAgent intelligentAgent = new DAMAgent(AttackVector);

			while (currentGame < GameCount)
			{
				// Attacker doesn't have enough memory yet to choose based on previous games, default to uniform cost
				if (currentGame < attackerMemory)
				{
					ChooseAttacks(attackerStrategy);
					PreviousAttacks.Add(Attacked);
				}
				else
				{
					List<Attack> attacks = chooseAttacksWithMemory(attackerStrategy, attackerProbability);
					if (PreviousAttacks.Count == PreviousAttacks.Capacity)
					{
						PreviousAttacks.RemoveAt(0);
					}
					PreviousAttacks.Add(attacks);
					Attacked = attacks;
				}

				// Defender doesn't have enough memory yet to choose based on previous games, default to uniform cost
				if (currentGame == 0)
				{
					List<Attack> defend = intelligentAgent.ChooseDefense();
					PreviousDefends.Add(defend);
					Defended = defend;

				}
				else
				{
					List<Attack> defend = intelligentAgent.ChooseDefense();
					if (PreviousDefends.Count == PreviousDefends.Capacity)
					{
						PreviousDefends.RemoveAt(0);
					}

					PreviousDefends.Add(defend);
					Defended = defend;
				}

				AttackerCosts += Attacked.Sum(a => a.costAttack);
				DefenderCosts += Defended.Sum(d => d.costDefend);

				AttackerPayoffs.Add(AttackerNetPayoff);
				DefenderPayoffs.Add(DefenderNetPayoff);

				intelligentAgent.UpdateKnownAttacks(Attacked); // After round is finish agent stores the attacks seen
				currentGame++;
			}

		}

		/// <summary>
		/// Chooses the attacks that the attacker will execute.
		/// </summary>
		/// <param name="strategy">The strategy that the attacker will use to choose the attacks.</param>
		/// <exception cref="ArgumentException">strategy value must be a non-negative number.</exception>
		public override void ChooseAttacks(int strategy)
		{
			if (strategy < 0)
			{
				throw new ArgumentException("strategy value must be a non-negative number.");
			}

			List<Attack> allAttacks = new List<Attack>(AttackVector);
			List<Attack> attacks = new List<Attack>();

			for (int ii = 0; ii < strategy; ii++)
			{
				if (allAttacks.Count == 0)
				{
					break;
				}
				// Generate a random element to skip to and choose
				int skip = DamRandom.Next(0, allAttacks.Count);
				Attack choice = allAttacks.Skip(skip).Take(1).First();

				// Remove the choice from the pool and add it to the attacker's list
				allAttacks.Remove(choice);
				attacks.Add(choice);
			}

			Attacked = attacks;
		}

		/// <summary>
		/// Chooses the attacks that the defender will try to defend against.
		/// </summary>
		/// <param name="strategy">The strategy that the defender will use to choose the attacks to defend against.</param>
		/// <exception cref="ArgumentException">strategy value must be a non-negative number.</exception>
		public override void ChooseDefense(int strategy)
		{
			if (strategy < 0)
			{
				throw new ArgumentException("strategy value must be a non-negative number.");
			}

			List<Attack> allAttacks = new List<Attack>(AttackVector);
			List<Attack> defense = new List<Attack>();

			for (int ii = 0; ii < strategy; ii++)
			{
				if (allAttacks.Count == 0)
				{
					break;
				}
				// Generate a random element to skip to and choose
				int skip = DamRandom.Next(0, allAttacks.Count);

				Attack choice = allAttacks.Skip(skip).Take(1).FirstOrDefault();

				// Remove the choice from the pool and add it to the defender's list
				allAttacks.Remove(choice);
				defense.Add(choice);
			}

			Defended = defense;
		}

		/// <summary>
		/// Chooses the attacks with respect to the attacker's memory of previous games.
		/// </summary>
		/// <param name="strategy">The strategy, in number of attacks to choose</param>
		/// <param name="probability">The probability of choosing an attack from a previous game in memory</param>
		/// <returns></returns>
		private List<Attack> chooseAttacksWithMemory(int strategy, float probability)
		{
			if (strategy < 0)
			{
				throw new ArgumentOutOfRangeException("strategy", "strategy should be nonnegative");
			}
			if (probability < 0 || probability > 1)
			{
				throw new ArgumentOutOfRangeException("probability", "probability should be be in the range [0,1]");
			}

			List<Attack> previous = PreviousAttacks.SelectMany(previousAttack => previousAttack).ToList();
			List<Attack> notPrevious = AttackVector.Except(previous).ToList();
			List<Attack> attacks = new List<Attack>();

			for (int i = 0; i < strategy; i++)
			{
				bool useMemory = DamRandom.NextDouble() <= probability;

				if (useMemory)
				{
					if (previous.Count == 0)
					{
						break;
					}
					// Generate a random element to skip to and choose
					int skip = DamRandom.Next(0, previous.Count);
					Attack choice = previous.Skip(skip).Take(1).First();

					// Remove the choice from the pool and add it to the attacker's list
					previous.Remove(choice);
					attacks.Add(choice);
				}
				else
				{
					if (notPrevious.Count == 0)
					{
						break;
					}
					// Generate a random element to skip to and choose
					int skip = DamRandom.Next(0, notPrevious.Count);
					Attack choice = notPrevious.Skip(skip).Take(1).First();

					// Remove the choice from the pool and add it to the attacker's list
					notPrevious.Remove(choice);
					attacks.Add(choice);
				}
			}

			return attacks;
		}

		/// <summary>
		/// Chooses the attacks with respect to the attacker's memory of previous games.
		/// </summary>
		/// <param name="strategy">The strategy, in number of attacks to choose</param>
		/// <param name="probability">The probability of choosing an attack from a previous game in memory</param>
		/// <returns></returns>
		private List<Attack> chooseDefenseWithMemory(int strategy, float probability)
		{
			if (strategy < 0)
			{
				throw new ArgumentOutOfRangeException("strategy", "strategy should be nonnegative");
			}
			if (probability < 0 || probability > 1)
			{
				throw new ArgumentOutOfRangeException("probability", "probability should be be in the range [0,1]");
			}

			List<Attack> previous = PreviousDefends.SelectMany(previousDefend => previousDefend).ToList();
			List<Attack> notPrevious = AttackVector.Except(previous).ToList();
			List<Attack> defends = new List<Attack>();

			for (int i = 0; i < strategy; i++)
			{
				bool useMemory = DamRandom.NextDouble() <= probability;

				if (useMemory)
				{
					if (previous.Count == 0)
					{
						break;
					}
					// Generate a random element to skip to and choose
					int skip = DamRandom.Next(0, previous.Count);
					Attack choice = previous.Skip(skip).Take(1).First();

					// Remove the choice from the pool and add it to the defender's list
					previous.Remove(choice);
					defends.Add(choice);
				}
				else
				{
					if (notPrevious.Count == 0)
					{
						break;
					}
					// Generate a random element to skip to and choose
					int skip = DamRandom.Next(0, notPrevious.Count);
					Attack choice = notPrevious.Skip(skip).Take(1).First();

					// Remove the choice from the pool and add it to the defender's list
					notPrevious.Remove(choice);
					defends.Add(choice);
				}
			}

			return defends;
		}
	}
}
