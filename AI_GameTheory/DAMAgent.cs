﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AI_GameTheory
{
	class DAMAgent
	{
		private const int STRATEGY_MEM_SIZE = 100;
		private const int DEFAULT_DEFENCE_STRAT = 2;
		private List<BaseFraudGameModel.Attack> allAttacks;
		private List<BaseFraudGameModel.Attack> knownAttacks;
		private List<BaseFraudGameModel.Attack> previousRoundAttacks;
		private List<int> attacksPerRoundList;

		private static Random _agentRandom;

		public static Random AgentRandom
		{
			get
			{
				if (_agentRandom == null)
				{
					_agentRandom = new Random();
					return _agentRandom;
				}
				return _agentRandom;
			}
		}

		public DAMAgent(List<BaseFraudGameModel.Attack> listAttacks)
		{
			allAttacks = listAttacks;
			knownAttacks = new List<BaseFraudGameModel.Attack>();
			previousRoundAttacks = new List<BaseFraudGameModel.Attack>();
			attacksPerRoundList = new List<int>(STRATEGY_MEM_SIZE);
		}

		private float calculateAttackProbability()
		{
			if (knownAttacks.Count == 0) // return 0% chance of repeat attack if no attacks have occured
			{
				return 0.0f;
			}

			List<BaseFraudGameModel.Attack> repeatAttacks = previousRoundAttacks.Intersect(knownAttacks).ToList();
			float attackProbability = (float)repeatAttacks.Count / knownAttacks.Count;
			return 1-attackProbability;
		}

		private int calculateDefenseStrategy()
		{
			if (previousRoundAttacks.Count > 0)
			{
				attacksPerRoundList.Add(previousRoundAttacks.Count);
				int averageAttacks = (int)attacksPerRoundList.Average(a => a);
				int calculated = averageAttacks*10;
				return (calculated < allAttacks.Count) ? calculated : allAttacks.Count;
			}
			else // If no attacks were performed in the previous rounds then revert to default
			{
				return DEFAULT_DEFENCE_STRAT;
			}
		}

		public void UpdateKnownAttacks(List<BaseFraudGameModel.Attack> attacks)
		{
			previousRoundAttacks = attacks;
			knownAttacks = knownAttacks.Union(attacks).ToList();
		}

		public List<BaseFraudGameModel.Attack> ChooseDefense()
		{
			int defStrategy = calculateDefenseStrategy();
			float estimatedAtkProbability = calculateAttackProbability();

			List<BaseFraudGameModel.Attack> previous = new List<BaseFraudGameModel.Attack>(knownAttacks);
			List<BaseFraudGameModel.Attack> notPrevious = allAttacks.Except(previous).ToList();
			List<BaseFraudGameModel.Attack> defend = new List<BaseFraudGameModel.Attack>();

			for (int i = 0; i < defStrategy; i++)
			{
				bool useMemory = AgentRandom.NextDouble() <= estimatedAtkProbability;


				if (useMemory)
				{
					BaseFraudGameModel.Attack choice;
					int skip;
					if (previous.Count == 0)
					{
						if (notPrevious.Count == 0)
						{
							break;
						}
						skip = AgentRandom.Next(0, notPrevious.Count);
						choice = notPrevious.Skip(skip).Take(1).First();
						// Remove the choice from the pool
						notPrevious.Remove(choice);
					}
					else
					{
						// Generate a random element to skip to and choose
						skip = AgentRandom.Next(0, previous.Count);
						choice = previous.Skip(skip).Take(1).First();
						previous.Remove(choice);
					}

					// Remove the choice from the pool and add it to the attacker's list
					defend.Add(choice);

				}
				else
				{
					if (notPrevious.Count == 0)
					{
						break;
					}
					// Generate a random element to skip to and choose
					int skip = AgentRandom.Next(0, notPrevious.Count);
					BaseFraudGameModel.Attack choice = notPrevious.Skip(skip).Take(1).First();

					// Remove the choice from the pool and add it to the attacker's list
					notPrevious.Remove(choice);
					defend.Add(choice);
				}
			}
			return defend;
		}
	}
}
