﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI_GameTheory;

namespace DAMSimulation
{
	class Program
	{
		public static int attackVectorLength = 100;
		public static int simulationRuns = 1000;
		public static int gamesPerSimulation = 500;
		public static int attackerStrat = 5;
		public static int defenderStrat = 5;
		public static int attackerMem = 5;
		public static int defenderMem = 5;
		public static float attackerProb = 0f;
		public static float defenderProb = 0f;
		public static DateTime StartTime;
		public static DateTime EndTime;

		static void Main(string[] args)
		{

			BaseFraudGameModel.Attack profile = new BaseFraudGameModel.Attack(0, 100, 100, 10000, 0);

			List<long> AttackerAvgPayoff = new List<long>();
			List<long> DefenderAvgPayoff = new List<long>();
			List<long> AttackerAgentAvgPayoff = new List<long>();
			List<long> DefenderAgentAvgPayoff = new List<long>();


			Console.WriteLine("Deterministic Adversary with Memory (DAM) simulation run started...\n");
			Console.WriteLine("Attack vector length: {0}", attackVectorLength);
			Console.WriteLine("Attacker strategy: {0}, and probability of using memory: {1}", attackerStrat, attackerProb);
			Console.WriteLine("Defender strategy: {0}, and probability of using memory: {1}", defenderStrat, defenderProb);
			Console.WriteLine("Attacker memory: {0}", attackerMem);
			Console.WriteLine("Defender memory: {0}", defenderMem);
			Console.WriteLine("Simulations: {0}, at {1} games per simulation", simulationRuns, gamesPerSimulation);

			StartTime = DateTime.Now;



			for (int i = 0; i < simulationRuns; i++)
			{


				DeterministicAdversaryWithMemory dam = new DeterministicAdversaryWithMemory(gamesPerSimulation, attackVectorLength, profile, attackerMem, defenderMem);
                DeterministicAdversaryWithMemory damWithAgent = new DeterministicAdversaryWithMemory(gamesPerSimulation, attackVectorLength, profile, attackerMem, defenderMem);
				dam.RunSimulation(attackerStrat, attackerProb, defenderStrat, defenderProb);
				damWithAgent.RunAdvancedSimulation(attackerStrat, attackerProb);

				//Console.WriteLine("Attacker payoff: {0}", dam.AttackerPayoffs);
				//Console.WriteLine("Attacker cost: {0}", dam.AttackerCosts);
				//Console.WriteLine("Defender payoff: {0}", dam.DefenderPayoffs);
				//Console.WriteLine("Defender cost: {0}", dam.DefenderCosts);


				//Console.WriteLine("\n        Net payoffs (attacker / defender): {0} / {1}", dam.AttackerNetPayoff, dam.DefenderNetPayoff);
				//Console.WriteLine("Agent - Net payoffs (attacker / defender): {0} / {1}", damWithAgent.AttackerNetPayoff, damWithAgent.DefenderNetPayoff);
			
				AttackerAvgPayoff.Add(dam.AttackerNetPayoff);
				DefenderAvgPayoff.Add(dam.DefenderNetPayoff);
				AttackerAgentAvgPayoff.Add((long) damWithAgent.AttackerPayoffs.Average());
				DefenderAgentAvgPayoff.Add((long) damWithAgent.DefenderPayoffs.Average());

				
			}

			EndTime = DateTime.Now;
			TimeSpan span = EndTime - StartTime;

			Console.WriteLine("Net payoff averages over {0} simulations ({1} ms):\n", simulationRuns, span.TotalMilliseconds);
			Console.WriteLine("No agent:\tattacker / defender = {0} / {1}", AttackerAvgPayoff.Average(), DefenderAvgPayoff.Average());
			Console.WriteLine("With agent:\tattacker / defender = {0} / {1}", (int) AttackerAgentAvgPayoff.Average(), (int) DefenderAgentAvgPayoff.Average());

			Console.ReadKey();
		}
	}
}
