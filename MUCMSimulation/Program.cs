﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI_GameTheory;

namespace MUCMSimulation
{
	class Program
	{

		public static int attackVectorLength = 100;
		public static int attackerPayoff = 10000;
		public static int defenderPayoff = 0;
		public static int attackerCost = 1000;
		public static int defenderCost = 100;
		public static int attackerStrategy = 20;
		public static int defenderStrategy = 80;

		public static int gameCount = 500;
		public static DateTime StartTime;
		public static DateTime EndTime;
		static void Main(string[] args)
		{
			List<int> AttackerPayoffs = new List<int>(gameCount);
			List<int> AttackerCosts = new List<int>(gameCount);
			List<int> DefenderPayoffs = new List<int>(gameCount);
			List<int> DefenderCosts = new List<int>(gameCount);

			int successfulAttacks = 0;

			BaseFraudGameModel.Attack profile = new BaseFraudGameModel.Attack(0, attackerCost, defenderCost, attackerPayoff, defenderPayoff);

            MixedUniformCostModel MUCM = new MixedUniformCostModel(10000, 100, profile);
            Tuple<int, int>[,] matrix = MUCM.GenerateAttackPayoffMatrix(attackerCost, attackerPayoff, defenderCost, defenderPayoff, attackVectorLength);


			Console.WriteLine("Mixed Uniform Cost Model (MUCM) simulation run started...\n");
			Console.WriteLine("Attack vector length: {0}", attackVectorLength);
			Console.WriteLine("Attacker cost per attack: {0}", attackerCost);
			Console.WriteLine("Defender cost per attack: {0}", defenderCost);
			Console.WriteLine("Attacker payoff per attack: {0}", attackerPayoff);
			Console.WriteLine("Defender payoff per attack: {0}", defenderPayoff);
			Console.WriteLine("Attacker strategy: {0}", attackerStrategy);
			Console.WriteLine("Defender strategy: {0}", defenderStrategy);
			Console.WriteLine("Simulations: {0}", gameCount);

			StartTime = DateTime.Now;

			for (int i = 0; i < gameCount; i++)
			{
				MixedUniformCostModel model = new MixedUniformCostModel(attackerPayoff, attackVectorLength, profile);
				model.ChooseAttacks(attackerStrategy);
				model.ChooseDefense(defenderStrategy);

				successfulAttacks += model.SuccessfulAttacks.Count;
				AttackerPayoffs.Add(model.AttackerNetPayoff);
				AttackerCosts.Add(model.SumAttackerCosts);
				DefenderPayoffs.Add(model.DefenderNetPayoff);
				DefenderCosts.Add(model.SumDefenderCosts);
			}

			EndTime = DateTime.Now;
			TimeSpan span = EndTime - StartTime;

			Console.WriteLine("\nNet payoff averages over {0} simulations ({1} ms):\n", gameCount, span.TotalMilliseconds);
			Console.WriteLine("Total attacks by attacker: {0}", attackerStrategy * gameCount);
			Console.WriteLine("Total defends by defender: {0}", defenderStrategy * gameCount);
			Console.WriteLine("\nTotal attacker cost: {0}", AttackerCosts.Sum());
			Console.WriteLine("Total defender cost: {0}", DefenderCosts.Sum());
			Console.WriteLine("\nAverage net attacker payoff: {0}", AttackerPayoffs.Average());
			Console.WriteLine("Average net defender payoff: {0}", DefenderPayoffs.Average());

			Console.ReadKey();
		}
	}
}
